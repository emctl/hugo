## Dashboard

The website is meant to serve as my documentation on my [GitOps](https://www.weave.works/technologies/gitops/) journey across my static homelab and dynamic "cloudlab". Everything should be "as code" meaning everything I do should be somewhere in my repos (apart from secrets hopefully :) )

I will try to commit myself to a post a week once everything is setup correctly and I get into the swing of things. If you have any suggestions for topics, please send a mail to my gitlab account which will create a ticket for me(my main method of communication for work regarding the blog)

Please take a look at some of my posts and most importantly - tell me what you think, I love getting feedback on how to improve.
