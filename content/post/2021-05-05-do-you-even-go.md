---
title: Do you even Go here?
subtitle: Golang that is
date: 2021-05-05
tags: ["Golang", "code"]
---

# Devops Journey
For a lot of companies, the role "DevOps Engineer" can mean a lot of different roles. I have had friends interview for a DevOps role that turned out to be a python developer (who were deploying to the cloud), another one had a similar experience where they went to a DevOps role that was effectivly a DBA (which didn't include one mention of SQL in the job spec). Makes perfect sense, right?

What does seem to be somewhat common is the need for some programming language. I had a quick look before starting this post, Python or Golang(aka Go) appeared in **ALL** my searches.

## Which one is better?
For a DevOps engineer, (in my experience anyway) I see Python, Go and Ruby as the main languages to learn. Now I am the furthest thing from a full time developer in any of those languages, however I can read them and make small changes when needed (to a degree that is) and I am activley trying to learn some Golang in my spare time. I personally think that having Python and Golang in your "programming languages I know" bag is all that most devops engineers should need.

Yes - I can hear you shouting "Jenkins is on groovy", "app x is on language y". Of that I do not disagree, however I would never confine myself to learning a language just for **one application** - I would learn for the biggest bang for buck. Go is used everywhere in "The Kubernetes Land"  and python is as common as electricity these days - everyone has had some dealing with it or used it on some project.

## Background before I start learning
Great question! To give you my background - My undergrad is in computer science where I mostly studied in Java. I have worked on a number of projects outside work with python but nothing too advanced (or that I can remember for that matter). I use this preface to say that I understand the basic basic concepts (What is an Int vs double, signed vs unsigned etc) however if you asked me to "develop an application in Golang that does Y" - I would be in trouble.

## Let's start!
Ok, enough disclaimers - lets do this. How?
- **Understand your starting point** - Accept that you don't know everything and do a check to see at what level you do know. Some people have never touched code and felt like they could never do it because they would be ridiculed. To that I say - Do you want to live inside your head or in the real world? You alone are in the driving seat to make the difference! 
- **Commit to a set of SMART goals** - Maybe [SMART](https://en.wikipedia.org/wiki/SMART_criteria) might be overkill but you need to make time to learn something - much like learning how to play an instrument, you can't read the theory and expect to know everything. Setup a plan of **what** you are going to learn followed by **when** you are going to learn it.
- **Be awesome** - You know what you need to learn, you know when you as going to spend time to learn it - now you need to execute on that. see my [commit challenge](https://emctl.gitlab.io/hugo/post/2021-04-12-the-commit-challenge/) for an example of what I have set out to do if you want some inspiration.

## Next steps
I plan to take you along the way on my journey - All the links I have used, all the public repos I will create and any fun developments along the way. Please let me know your thoughts and if you have any tips for me!
