---
title: iPad laptop?
subtitle: iCant - not fully
date: 2021-05-05
tags: ["iPad", "gitlab"]
---

# Laptop Replacement
I have toyed with the idea of using "x" as my daily driver as I try to push my workflows online where possible - I tried with my chromebook (the lenovo duet) but was blocked with a bad hardware experience, however it's a super budget device so I expected to make some trade offs. With the new iPad pro 2021, I thought hardware certainly wouldnt be a problem, so could I use the ipad as my daily driving. This blog post is to function as my first weeks notes.

**TL;DR** - Gitlab web ide doesnt play nice with the iPad which makes blog posts an issue but it's great for most other things

## Pros of using iPad
- **Battery Life** - Lets see it like it is, an iPad will probably outlast most laptops in terms of battery life. Now I not saying anything bad about your laptop - just pointing out that this hardware will get you through your day and then some.

- **Mobility** - Yes I know what your thinking - "I can move my laptop around too", but I meant this in more than just your mobility. There is something about being able to transition from a tablet that weighs nothing (almost) to a "docked" laptop (if you shell out to get a keyboard, e.g. [the apple magic keyboard](https://www.apple.com/uk/shop/product/MXQT2B/A/magic-keyboard-for-ipad-pro-11-inch-3rd-generation-and-ipad-air-4th-generation-british-english-black)) that just seems nice. I never thought I would use the ipad in tablet only mode, but I can say that I personally use my iPad in both docked and undocked modes daily.

------------------------------------

## Cons of the iPad
- **Copy and paste not working at present** (in the gitlab Web IDE) - At first I was unsure if this was a problem with iOS or Gitlab Web IDE but after some testing it looked to be with Gitlab Web IDE as I checked on other sites with no issues. Will look to see if there is any open gitlab issues / workarounds. (still on my TODO - will update it here with the link once I check it out) 

- **Reliance on gitpod** vs using the web IDE for simple blogs or quick adds of a similar block of text, which isnt great - however I could use something like [drafts](https://getdrafts.com/) or [notion](https://www.notion.so/) which also does markup but may be an issue when trying to render images as the links wouldn't exist in those accounts (only on the gitlab repo)

