---
title: What Language to learn? 
subtitle: for cloud natives
date: 2021-07-01
tags: ["programming", "golang", "python"]
---

# Who cares?
Start with the most obvious question, right? 
- Why is this even something to think about? Just pick any. 
- You need language x. 


I get it.

We all have opinions, some people really dont care, some people will stick to a single language that they learnt in college. Good for them.

The purpose of this article is more about my personal experinces, and why I didnt stick to what I learned in college.

It may come as no surprise in the DevOps community that my two languages of choice are....

**Insert drum roll here**

Python and Golang!

&nbsp;
# Python

**Pro's**
- Easy to get to grips with - It's no surprise that a large number of newcomers start their programming journeys with python. Python is your friend.

- Used in a lot of teams / companies - Have a look at any devops job going today, python is usually up there in the list of programming languages they would expect you to learn.

- 100's of free tutorials - One of the best things about python is the sheer number of tutorials and guides available. Got a super crazy error that you've know idea where to start? Theres a guide for that.

**Con's**
- Easy to get away with bad pactices - When something works in Python (somehow) that doesnt make it transferable to other languages (Python assumed you meant to return an int, so it makes your variable of type int, other languages won't let you get away with that)

- Can form a reliance on libraries - What can be a super duper benefit (it really is!), it can also be an issue when looking to move to other languages and you want to rewrite a piece of code

&nbsp;
# Golang
**Pro's**
- Super fast - Once your awesome codebase starts to grow, speed may become an issue. If you never do anything other than hello world, it shouldn't really matter but always good to keep speed in mind.

- Built in features are pretty cool (fmt / test / build) - Yes I know you can do this functionality with pretty much every other language but it's nice that it's built in.

- Lots of cloud native projects are based on Go - Kubernetes, prometheus, "insert most cncf projects here"

**Con's**
- Higher learning curve than python - Yea, this is certainly something to be aware of, this ain't python, you need to pay more attention to what your code is doing (e.g. do you want int16? int 32? int 8? don't know what you want? RTM :) )

&nbsp;
# Other options

These are quite popular from what I have witnessed however I can't say at present much about them without getting some hands on time first, which at present I am spending more of my time with Go. Maybe sometime this year, just not at the moment.

- Rust 
- Typescript
