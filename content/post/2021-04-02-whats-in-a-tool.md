---
title: What tool?
subtitle: and why
date: 2021-04-02
tags: ["tools", "opensource"]
---

# How to pick a good GitOps tool

## Clearly defined requirement

What problem does this tool solve? If thats not something obvious, maybe you should take a step back and get clearly defined requirements to the problem, otherwise you can be stuck in tool limbo, moving from tool to tool, never feeling at home (ok maybe not that extreme but hopefully you get my point)

## Open Source 

I'm sure you saw this one coming - gotta support those open source warriors! Not only is it transparent and low cost (usually no-cost but donating to give the devs a kick back never hurts) but also you have the power to make the tool better and benefit everyone, how cool is that? You get to learn and contribute to a tool to the benefit of everyone (**if** you want to, you are not forced to in anyway)

## Statefiles 

While I have a lot of love for [terraform](terraform.io), in terms of a gitops workflow, I've have my fair share of statefile locks and issues with trying to run minor changes often (requiring spinning up x amount of dependent resources) I feel like it seems like a bit of an anti-pattern to have everything in code but also needing to maintain a statefile outside of code. At the sacrifice of stability, I like to try out tools that are fully on the gitops train. [Crossplane](crossplane.io) has been something I've been keeping my eye on for some time however it's not without issue (lets save that for another post)

## Vendor Neutral

While this can usually be paired quiet closly to my point on open source, I want to ensure I cover any fringe cases. This goes for cloud platforms too. For example, I prefer [vault](https://www.vaultproject.io/) to [AWS Secret Manager](https://aws.amazon.com/secrets-manager/) since it plays nice with all cloud providers (that I use anyway) and on-prem.

## Community 

Whilst this also may seem similar to my point on OSS - You can have an open source project with a shrinking community thats moving to a different tool and you could have a deprecated tool. It can be quite hard to predict the future (but never stop trying) I usually try to have a look at the issues (how often they come in, how many comments, how many are being closed in what cadence), commits (similar to issues, how many people are interacting and how often), stars and forks (not much of an indicator as issues but show some level of popularity)

## Bonus: CNCF project

Think of a tool you are thinking about using in your gitops journey - can't think of any but you know the problem you are trying to solve?
Chances are, the [cncf](https://www.cncf.io/) has your back. Have a look [here](https://landscape.cncf.io/) and just take in the breath and depth of tools at your disposal. Everything links back to the source code, you can comment / download as you see fit. if you have slack - check out the cncf [slack](https://slack.cncf.io/) for a taste of the great people supporting the community.
