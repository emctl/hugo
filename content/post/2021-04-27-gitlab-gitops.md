---
title: Gitlab for Greatness
subtitle: and GitOps
date: 2021-04-27
tags: ["tools", "opensource"]
---

# Why on earth use Gitlab?

## Why not?
What other tools are there? how many other tools does it replace? (spoiler - loads!) - I get the jack of all trades, master of none however in my personal experience, everything has been up to the feature set I need. If I only need 80% of what the other tools do and Gitlab already does that - why bother looking?

## Monthly Releases
If a feature doesn't exist - there is a consistent push to make things happen, for the last [115 months](https://about.gitlab.com/releases/) they have been pushing out new releases.

## Strength to Strength
Releated to the above Monthly Releases section but Gitlab only seems to get better - anything thats buggy, make an issue - people do respond, or better yet - suggest a fix and make your PR - Your changes could make it better for everyone!

## Transparency 
One for Gitlab's [Core Values](https://about.gitlab.com/handbook/values/) is transparency, This can certainly be seen from spending anytime with gitlab. Issues, discussion - everything is open for everyone - and thats great!
