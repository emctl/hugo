---
title: Introduction to IaC 
subtitle: Infrastructure as Code
date: 2021-04-05
tags: ["IaC", "code"]
---

Microsoft describes IaC as:
Infrastructure as Code (IaC) is the management of infrastructure (networks, virtual machines, load balancers, and connection topology) in a descriptive model, using the same versioning as DevOps team uses for source code. Like the principle that the same source code generates the same binary, an IaC model generates the same environment every time it is applied. IaC is a key DevOps practice and is used in conjunction with continuous delivery.

# What exactly does that mean?

In the same way you would commit your programming code to a repo (think [gitlab](https://gitlab.com/) or [github]([gitlab](https://github.com/))), IaC is a method to do the same with your infrastruture. rather than configuring every single virtual (or pyhiscal bare metal machines for that matter) you every have, what if you could add a snippet of code that would allow you to configure those machines in an automated way?
Thats exactly what IaC tries to do - checkout [terraform](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/virtual_machine#example-usage) if you'd like to see an example. [Terraform](https://www.terraform.io/) is a popular tool used for IaC with a ton of examples to configure all sorts of infrastructure.  

# Why are you telling me this?

Why not? :)
Apart from the nerdy benefits of new stuff (my excuse for learning new things in IT) - As part of this website's goals to document my GitOps journey, IaC is part of this journey, since we try to do EVERYTHING via code - from networkings all the way up to our applications / monitoring. With that - I think it's a worthwhile idea to take some time to add a quick note on IaC. Some readers my just roll their eyes (obvious to some but not everyone) when talking about IaC however better not to assume (as we all know what [assuming](https://en.wikipedia.org/wiki/Jerry_Belson) does, right? )

# Closing Remarks

This is a short introduction into IaC - as the project matures, so should the documentation (however should something be amiss - just remember, everything is open to a merge request - (and I welcome them) ) - This is one of the first posts that was committed, which should highlight the level I am trying to start documenting at.
