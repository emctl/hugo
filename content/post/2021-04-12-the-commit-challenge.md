---
title: The Commit Challenge
subtitle: Because every task should be a challenge
date: 2021-04-12
tags: ["Git", "code", "habit"]
---

During this [pandemic](https://en.wikipedia.org/wiki/COVID-19_pandemic), many people have tried to take up new things they never had time for. Some took up painting, others took up baking (thanks [GBBO](https://en.wikipedia.org/wiki/The_Great_British_Bake_Off)) but me - I tried to do some learning (yes, how nerdy - I know).

As part of my learning, I did some certifications (all of which where AWS) but theres only some much book study you can do before the need to get back into homelabbing takes over, so with that - here I am. I wanted to apply some methods I used for study to how I was ~~building~~ trying to build my homelab.
One of those methods, which may sound very obvious to some people (and every gym goer ever) is **little and often**. How could I keep doing small and incemental updates to a homelab? The Git challenge was born

# The Git Challenge
![title](content/images/commit_history.png)

[Gitlab](https://gitlab.com/) has a cool little heatmap that gives a visual queue everytime you make a commit. With everything [as code](content/post/2021-04-05-intro-to-as-code.md) means pretty much everything I can do in the homelab project, should have a commit attached (even VM creation with the help of [harvster](https://github.com/rancher/harvester) and [kubevirt](https://kubevirt.io/)) which in my case, motivate me to do "something" - be that opening a MR or creating an issue for something I forgot but the aim is the same -  **small incremental steps**

If I get a weekend where I have nothing else on my TODO list (spoiler - there always some other work to be done) then does it feel great - sure, but thats not a reason not to commit the next day.

## Benefits of daily commits

**Never more than 24 hours of brain fog** - Ever have those days of "what am I doing again"? - we all have, moreso after a long weekend or some time off. If you never take an extended break from your project, you are never too far away from "the pulse" of your project. This post for example - check out the commits, I didn't sit down and bust it out over a day but over a while (alongside some other posts / pieces so I wasn't full time blogging)

**Build good habits** - OK, good habits might be a strech here but you get into the habit of also working towards a goal, you will become consistent. If this is a "good" habit or not will be up for debate (depending on who you ask) but if you are reading this - you're in good company and we think it's a great habit to have.

**You breakdown hard problems** - ok, ok - before everyone gives me the "each story should be small enough in scope to the epic arguement" - I get it, yes you are correct, every story *should* (usually) be a small bite size chunk that rolls up to the epic **however** not everyone / everything is that binary - sometimes you don't want to get bogged down with writing stories (e.g. you only have x amount of time to actually dedicate to a task, you are the only person working on a project) There can be 101 reasons why you dont fall into this method of creating tickets.
Now - with all that aside, daily commits to a problem may keep the ball rolling. If you have good "commit hygeine", your comments for your commits should serve as part of your documentation when trying to work towards what seems like a huge task







